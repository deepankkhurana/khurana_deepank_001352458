/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;


/**
 *
 * @author vaibh
 */
public class PatientDirectory {
    
     private ArrayList <PatientRecord> patientarry;
      private ArrayList <VitalSigns> vitalSignarry;
     
     public PatientDirectory(){
         patientarry = new ArrayList();
         vitalSignarry = new ArrayList();
     }

    public ArrayList<PatientRecord> getpatientarry() {
        return patientarry;
    }

    public ArrayList<VitalSigns> getVitalSignarry() {
        return vitalSignarry;
    }

    public void setVitalSignarry(ArrayList<VitalSigns> vitalSignarry) {
        this.vitalSignarry = vitalSignarry;
    }
    
    

    public void setpatientarry(ArrayList<PatientRecord> patientarry) {
        this.patientarry = patientarry;
    }

     public void addPatient (PatientRecord ps){
         
         patientarry.add(ps);
     }
     
    /**
     *
     * @return
     */
     
    public PatientRecord newPatient(){
         PatientRecord ps12 = new PatientRecord();
         patientarry.add(ps12);
         return ps12;
      }
  
    
    
}
