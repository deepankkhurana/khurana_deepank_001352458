/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
/**
 *
 * @author vaibh
 */
public class VitalColor implements TableCellRenderer {

    private static final TableCellRenderer Renderer = new DefaultTableCellRenderer();
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        
Component clr = Renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

if (column ==0){
        Object result = table.getModel().getValueAt(row, column);
        Double average = Double.parseDouble(result.toString());
        Color color = null;
        if((average <20) || (average >40)){
            color = Color.red;
        }
        clr.setForeground(color);
}
else if (column ==1){
        Object result = table.getModel().getValueAt(row, column);
        Double average = Double.parseDouble(result.toString());
        Color color = null;
        if((average <10) || (average >40)){
            color = Color.red;
        }
        clr.setForeground(color);
}
else if (column ==2){
        Object result = table.getModel().getValueAt(row, column);
        Double average = Double.parseDouble(result.toString());
        Color color = null;
        if((average <80) || (average >130)){
            color = Color.red;
        }
        clr.setForeground(color);
}
else if (column ==3){
        Object result = table.getModel().getValueAt(row, column);
        Double average = Double.parseDouble(result.toString());
        Color color = null;
        if((average <120) || (average >160)){
            color = Color.red;
        }
        clr.setForeground(color);
}
else if (column ==4){
        Object result = table.getModel().getValueAt(row, column);
        Double average = Double.parseDouble(result.toString());
        Color color = null;
        if((average <60) || (average >80)){
            color = Color.red;
        }
        clr.setForeground(color);
}
else {
        clr.setForeground(Color.black);
}

return clr;
    }
    


    
}
