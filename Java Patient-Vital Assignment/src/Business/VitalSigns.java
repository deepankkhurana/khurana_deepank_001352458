/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

//import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.util.Date;

/**
 *
 * @author vaibh
 */
public class VitalSigns {
    private int respRate;
    private int heartRate;
    private double bloodPressure;
    private int weightPound;
    private int weightKilos;

    public int getRespRate() {
        return respRate;
    }

    public void setRespRate(int respRate) {
        this.respRate = respRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public double getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(double bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public int getWeightPound() {
        return weightPound;
    }

    public void setWeightPound(int weightPound) {
        this.weightPound = weightPound;
    }

    public int getWeightKilos() {
        return weightKilos;
    }

    public void setWeightKilos(int weightKilos) {
        this.weightKilos = weightKilos;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
    private Date timeStamp;
    
}
