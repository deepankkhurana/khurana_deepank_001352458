/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;

/**
 *
 * @author vaibh
 */
public class PatientRecord {
 
    private String name;
    private int id;
    private String doctorName;
    private String pharmacyName;
    private VitalSignList vitalSignnew;
    
    private  PatientRecord precord;
    
    public String getName() {
        return name;
    }

    private VitalSignList vsl;
    
    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPharmacyName() {
        return pharmacyName;
    }

    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    }
    
    public PatientRecord(){
        this.vsl = new VitalSignList();
        this.precord = precord;
        vitalSignnew = new VitalSignList();
    }
    
    public VitalSignList getVitalSignnew(){
        return vitalSignnew;
    }

    public void setVitalSignnew(VitalSigns vs1) {
      this.vitalSignnew = vitalSignnew;
    }

    @Override
    public String toString() {
        return this.name;
    }
    
}
